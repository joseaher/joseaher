# Summary

I have over eight years of research experience in astrophysics, I got my PhD in 2015, I have a robust background in astronomical data analysis, scientific inference, and software development. My expertise encompasses database management, graphical interface design, and the implementation of advanced data analysis and visualization tools.



Throughout my career, I have honed my skills in Python and its numerous libraries (Numpy, Scipy, Matplotlib, Pandas, Seaborn, Bokeh, Astropy), as well as in PowerBI, HTML, Linux, data analysis and visualization, numerical simulations, statistics, data structures and algorithms, shell scripting, and Git. I am currently expanding my knowledge in machine learning techniques, including regression, classification, and deep learning using Scikit-learn and TensorFlow.

I have developed several software packages that has contributed to the field of astrophysics:

- [ ] ASTROMORPHLIB ([GitLab](https://gitlab.com/joseaher/astromorphlib)): Python scripts for analyzing the morphology of isolated and interacting galaxies.
- [ ] ASTROCUBELIB ([GitLab](https://gitlab.com/joseaher/astrocubelib)): Routines for fitting lines in spectra from long-slit or integral field spectroscopy observations.
- [ ] ASTROISMLIB ([GitLab](https://gitlab.com/joseaher/astroismlib)): Tools for plotting grids of shock and ionization models in BPT diagrams, estimating dust extinction, star formation rates, ionizing photons, and more.
- [ ] ASTROMODELLINGLIB ([GitLab](https://gitlab.com/joseaher/astroismlib)): Routines for modeling the kinematics, dynamics, and morphology of galaxies.
- [ ] ASTROPLOTLIB ([GitLab](https://gitlab.com/joseaher/astroplotlib)): Tools for plotting astronomical images with various features.

I have also created intuitive graphical user interfaces for some of these software routines. My research includes papers utilizing Bayesian statistical inference for model parameter exploration and employing machine learning methods such as random forest regression for star and galaxy classification. Additionally, I have constructed interactive dashboards for data analysis.

Beyond my technical skills, I have a proven track record of community engagement through scientific presentations, teaching, outreach activities, and research seminars. I am fluent in Spanish, Portuguese, and English, and have extensive experience collaborating with diverse, multicultural teams.


My academic background and professional experiences have equipped me with a solid foundation in problem-solving and a curiosity-driven approach to learning new subjects. I am eager to leverage my expertise and offer a new perspective in solving problems in subjects outside Astronomy.


# Research interests

My research interests encompass a broad range of topics in astrophysics, focusing on the morphology, kinematics, and dynamics of isolated and interacting systems. Specifically, I am interested in:


- [ ] Kinematic Modeling: Investigating both large-scale and circum-nuclear galactic structures to understand their dynamic properties.
- [ ] Feedback Efficiency: Exploring the efficiency of feedback mechanisms in star-forming regions.
- [ ] Metallicity Gradients: Studying the metallicity gradients in interacting galaxies to gain insights into their chemical evolution.
- [ ] Dynamics of Hot Systems: Analyzing the dynamics of hot systems, including ellipticals, bulges, and star clusters.
- [ ] Dynamics of Star Cluster in Mcs: Probing the dynamics status of the star clusters in the outermost regions of Magellanic Clouds.
- [ ] Ram Pressure Stripping: Examining the processes and effects of ram pressure stripping in galaxies.


In addition to these areas, I have a strong interest in Computational Astrophysics, particularly in the following areas:

- [ ]Software Development: I am the author of five Python packages that provide tools for conducting astrophysical research across various topics.
- [ ] Numerical Simulations: Conducting N-body and Smoothed Particle Hydrodynamics (SPH) simulations to model astrophysical phenomena.
- [ ]Machine Learning Techniques: Applying machine learning methods to analyze and interpret astronomical data.


# Research Profile

Throughout my research career, I have published 31 papers in indexed international journals, with 475 citations on Web of Science, an H-index of [10](https://www.webofscience.com/wos/author/record/3801472), and 716 citations on Google Scholar, an H-index of [12](https://scholar.google.com/citations?user=5zomTr0AAAAJ&hl=pt-BR&oi=ao).

These are my research profiles with links to the published papers:

- [ ] [Google Scholar](https://scholar.google.com/citations?user=5zomTr0AAAAJ&hl=pt-BR&oi=ao)
- [ ] [ORCID](https://orcid.org/0000-0002-2925-1861)
- [ ] [Web of Science](https://www.webofscience.com/wos/author/record/3801472)
- [ ] [Research Gate](https://www.researchgate.net/profile/Jose-Hernandez-Jimenez-2)
